import { navbar } from "vuepress-theme-hope";

export default navbar([
  "/",
  {
    text: "全部招聘",
    icon: "book",
    link: "/call",
  },
  {
    text: "区域招聘",
    icon:"",
    link:"/city"
  },
  
]);
