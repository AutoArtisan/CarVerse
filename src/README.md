---
home: true
icon: home
title: 主页
heroImage: /logo.svg
# bgImage: https://theme-hope-assets.vuejs.press/bg/6-light.svg
# bgImageDark: https://theme-hope-assets.vuejs.press/bg/6-dark.svg
bgImageStyle:
  background-attachment: fixed
heroText: 上岸（NewLand）
tagline: 下一站，上岸。
actions:
  - text: 招聘公告
    icon: lightbulb
    link: ./call/
    type: primary

  - text: 作者简介
    link: ./city/


copyright: false
footer: 使用 <a href="https://theme-hope.vuejs.press/zh/" target="_blank">VuePress Theme Hope</a> 主题 |  Copyright © 2024 CarVerse
---

那些在深夜里睡不着的日子、那些面对深渊想要逃跑的瞬间，在日后看来都是非常珍贵的时刻，他让我们诚实地认识自己，只有认识他，接受他才能真正的超越他”。
每一个人，都必定出发于他的少年时，只是很多年以后，蓦然回首，你是否还会记得，那年少时的梦呢？

年少时的梦有时很幸运的实现了，有些被遗忘在了风中，做么时候有过什么要了一个梦其实并不是很重要，重要的是，我们曾经为了这个梦如此热烈的爱过，执着地追求过，勇敢地拼搏过，这是梦的意义，也是值得被 记住的时光。

欢迎加入我们，一起做有意义的事情。